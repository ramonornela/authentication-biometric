import { nanoid } from 'nanoid';

const STORAGE_KEY_BIOMETRIC = 'biometric_active';

const STORAGE_KEY_TOKEN = 'token';

export const buildSecret = () => nanoid();

export const activateBiometry = (activate = true) => {
  localStorage.setItem(STORAGE_KEY_BIOMETRIC, activate ? '1' : '0');
};

export const isActivateBiometry = () => {
  return !!parseInt(localStorage.getItem(STORAGE_KEY_BIOMETRIC));
}

export const setToken = (token: string) => localStorage.setItem(STORAGE_KEY_TOKEN, token);

export const getToken = () => localStorage.getItem(STORAGE_KEY_TOKEN);

export const removeToken = () => localStorage.removeItem(STORAGE_KEY_TOKEN);
