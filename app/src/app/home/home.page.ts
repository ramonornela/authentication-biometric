import { Component, OnInit } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { isActivateBiometry } from '../helpers';
import { FingerprintService, FlowService } from '../services';
import { removeToken } from '../helpers';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(
    private alertController: AlertController,
    private fingerprint: FingerprintService,
    private flow: FlowService,
    private router: Router,
    private toastController: ToastController
  ) {}

  ngOnInit() {
    this.presentAlert();
  }

  async presentAlert() {
    if (isActivateBiometry()) {
      return;
    }

    const isAvailable = await this.fingerprint.check();

    if (!isAvailable) {
      return;
    }

    const alert = await this.alertController.create({
      header: 'Biometria',
      subHeader: '',
      message: 'Deseja ativar biometria para este usuário.',
      buttons: [
        {
          text: 'Não',
          role: 'cancel'
        }, {
          text: 'Sim',
          handler: () => {
            this.flow.enableBiometry().then(async () => {
              (await this.createToast('Biometria ativada com sucesso')).present();
            });
          }
        }
      ]
    });

    await alert.present();
  }

  async logout() {
    const alert = await this.alertController.create({
      header: '',
      message: 'Deseja fazer logout?',
      buttons: [
        {
          text: 'Não',
          role: 'cancel'
        }, {
          text: 'Sim',
          handler: () => {
            removeToken();
            this.router.navigate(['/']);
          }
        }
      ]
    });

    await alert.present();
  }

  private async createToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });

    return toast;
  }
}
