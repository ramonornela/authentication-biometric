import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl , Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { ApiService, FlowService } from '../services';
import { getToken, setToken } from '../helpers';

const STORAGE_KEY_USERNAME = 'username';
@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage implements OnInit {

  form: FormGroup;

  constructor(
    private api: ApiService,
    private flow: FlowService,
    private router: Router,
    private loadingController: LoadingController,
    private toastController: ToastController
  ) {}

  ngOnInit() {
    const token = getToken();
    if (token) {
      this.redirectToHome();
      return;
    }
    this.initForm();
    this.flow.loginSecret(localStorage.getItem(STORAGE_KEY_USERNAME)).then(result => {
      if (result) {
        this.redirectToHome();
      }
    });
  }

  private validate() {
    return true;
  }

  async login() {
    if (!this.validate()) {
      return;
    }

    const username = this.form.get('email').value;

    const loading = await this.createLoading();

    loading.present();

    try {
      const result: any = await this.api.login(
        username,
        this.form.get('password').value
      );

      this.saveUserName(username);
      setToken(result.token);
      loading.dismiss();
      this.redirectToHome();
    } catch {
      loading.dismiss();
      (await this.createToast('Credenciais invalidas')).present();
    }
  }

  private initForm() {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    });
  }

  private redirectToHome() {
    this.router.navigate(['/home']);
  }

  private saveUserName(username: string) {
    localStorage.setItem(STORAGE_KEY_USERNAME, username);
  }

  private async createLoading(message?: string): Promise<any> {
    const loading = await this.loadingController.create({
      message: message ?? '',
      duration: 2000
    });

    return loading;
  }

  private async createToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });

    return toast;
  }
}
