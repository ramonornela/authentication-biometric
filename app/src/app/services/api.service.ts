import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { getToken } from '../helpers';

const API_BASE_URL = 'http://unipaps-dev.us-east-1.elasticbeanstalk.com';

@Injectable()
export class ApiService {

  constructor(private httpClient: HttpClient) {}

  async login(username: string, password: string) {
    const url = `${API_BASE_URL}/auth`

    const data = { username, password };

    return await this.httpClient.post(url, data).toPromise();
  }

  async loginSecret(username: string, secret: string) {
    const url = `${API_BASE_URL}/auth-secret`

    const data = { username, secret };

    return await this.httpClient.post(url, data).toPromise();
  }

  async biometryEnable(secret: string) {
    const url = `${API_BASE_URL}/biometry/enable`

    const data = { secret };

    return this.httpClient.post(
      url,
      data,
      { headers: this.getDefaultHeaders() }
    ).toPromise();
  }

  async biometryDisable() {
    const url = `${API_BASE_URL}/biometry/disable`

    return this.httpClient.delete(url, { headers: this.getDefaultHeaders() }).toPromise();
  }

  private getDefaultHeaders() {
    return {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${getToken()}`
    };
  }
}
