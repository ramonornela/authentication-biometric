import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { ApiService } from './api.service';
import { FingerprintService } from './fingerprint.service';
import { FlowService } from './flow.service';

@NgModule({
  imports: [HttpClientModule],
  providers: [
    ApiService,
    FingerprintAIO,
    FingerprintService,
    FlowService
  ]
})
export class ServiceModule {}
