import { Injectable } from '@angular/core';
import {
  FingerprintAIO,
  FingerprintOptions,
  FingerprintSecretOptions
} from '@ionic-native/fingerprint-aio/ngx';

@Injectable()
export class FingerprintService {

  private readonly title = 'APS Biometria';

  constructor(private fingerprint: FingerprintAIO) {}

  async register(secret: string) {
    await this.check();

    const options: FingerprintSecretOptions = {
      title: this.title,
      secret
    };

    return await this.fingerprint.registerBiometricSecret(options);
  }

  async load() {
    await this.check();

    const options: FingerprintOptions = {
      title: this.title,
    };

    return await this.fingerprint.loadBiometricSecret(options);
  }

  async check() {
    return await this.fingerprint.isAvailable();
  }
}
