import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { FingerprintService } from './fingerprint.service';
import {
  activateBiometry,
  buildSecret,
  isActivateBiometry
} from '../helpers';

@Injectable()
export class FlowService {

  constructor(
    private fingerprint: FingerprintService,
    private api: ApiService
  ) {}

  async loginSecret(username: string) {
    if (!isActivateBiometry()) {
      return false;
    }

    const secret = await this.fingerprint.load();

    await this.api.loginSecret(username, secret);

    return true;
  }

  async enableBiometry(): Promise<void> {
    if (isActivateBiometry()) {
      return;
    }

    const secret = buildSecret();

    await this.api.biometryEnable(secret);

    await this.fingerprint.register(secret);

    activateBiometry();
  }
}
