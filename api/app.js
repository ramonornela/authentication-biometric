const express = require('express');
const app = express();
const cors = require('cors');
const jwt = require('jsonwebtoken');
const fs = require('fs');

let session;

const updateData = (users) => fs.writeFileSync('./users.json', JSON.stringify(users));

const authMiddleware = (req, res, next) => {
    const authorization = req.get('Authorization');
    const token = authorization?.replace(/^Bearer ?/, '');

    if (!token) {
        res.status(401).send('Invalid auth');
        return;
    }

    if (!verifyToken(token)) {
        res.status(401).send('Invalid auth');
        return;
    }

    const [user, users] = getUser(user => user.api_token === token);

    session = { user, users };

    if (!user) {
        res.status(401).send('Invalid auth');
        return;
    }

    next();
};

app.use(express.json());
app.use(cors());

const getUser = condition => {
    const users = require('./users.json');
    let i = 0;
    let matchUser = null;
    let index = null;
    for (const user of users) {
        if (condition(user, i)) {
            matchUser = user;
            index = i;
            break;
        }

        i++;
    }

    return [matchUser, users, index];
};

const getToken = id => {
    const privateKey = fs.readFileSync('./keys/jwtRS256.key');

    return jwt.sign({ id }, privateKey, { algorithm: 'RS256', expiresIn: 60 * 60 });
}

const verifyToken = token => {
    const publicKey = fs.readFileSync('./keys/jwtRS256.key.pub');

    return jwt.verify(token, publicKey, { algorithm: 'RS256' });
}

const auth = (body, keyNameSecret) => {
    const [user, users] = getUser(user => {
        return user.email === body.username && user[keyNameSecret] === body[keyNameSecret];
    });
    
    if (!user) {
        return false;
    }

    user.api_token = getToken(user.uuid);

    updateData(users);

    return user;
};

app.post('/auth', (req, res) => {
    const body = req.body;
    const user = auth(body, 'password');
    if (!user) {
        res.status(401).send('Invalid credentials');
        return;
    }

    res.status(200).json({ token: user.api_token });
});

app.post('/auth-secret', (req, res) => {
    const body = req.body;
    const user = auth(body, 'secret');
    if (!user) {
        res.status(401).send('Invalid credentials');
        return;
    }

    res.status(200).json({ token: user.api_token });
});

app.post('/biometry/enable', authMiddleware, (req, res) => {
    const body = req.body;
    if (!body.secret) {
        res.status(400).send('Invalid secret');
        return;
    }

    session.user.secret = body.secret;

    updateData(session.users);

    res.json({ok: true});
});

app.post('/biometry/disable', authMiddleware, (req, res) => {
    const body = req.body;
    if (!auth(body, 'secret')) {
        res.status(401).send('Invalid credentials');
        return;
    }

    res.status(200).send('OK');
});

app.get('/ping', (_, res) => {
    res.send('Pong');
});

app.listen(8080, () => {
    console.log('server started');
});
